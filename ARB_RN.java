
/*
		nom : NAJIHI
		prenom : Abdelilah
	
	
*/

import java.awt.Color;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * <p>
 * Implantation de l'interface Collection basée sur les arbres binaires de
 * recherche. Les éléments sont ordonnés soit en utilisant l'ordre naturel (cf
 * Comparable) soit avec un Comparator fourni à la création.
 * </p>
 * 
 * <p>
 * Certaines méthodes de AbstractCollection doivent être surchargées pour plus
 * d'efficacité.
 * </p>
 * 
 * @param <E>
 *            le type des clés stockées dans l'arbre
 */


public class ARB_RN<E> extends AbstractCollection<E> {
    public Noeud racine;
    private int taille;
    private Comparator<? super E> cmp;
    private Noeud sentinelle;


    private class Noeud {
        E cle;
        Noeud gauche;
        Noeud droit;
        Noeud pere;
        Color couleur;

        Noeud() {
            this.cle = null;
            this.gauche = null;
            this.droit = null;
            this.pere = null;
            this.couleur = Color.BLACK;
        }

        //Constructeur avec la clé en parametre
        Noeud(E cle) {
            couleur = Color.red;
            this.cle = cle;
            gauche = sentinelle;
            droit = sentinelle;
            pere = sentinelle;
        }

        /** oui c'est le noeud est null
         * @return un boulean
         */
         //Renvoie true or fals si le noeud courant est sentinelle ou bien no

        boolean isSentinelle() {
            return this == ARB_RN.this.sentinelle;
        }

        /**
         * Renvoie le noeud contenant la clé minimale du sous-arbre enraciné dans ce noeud
         *
         * @return le noeud contenant la clé minimale du sous-arbre enraciné dans ce noeud
         *
         */
        Noeud minimum() {
            Noeud node = this;
            while (node.gauche != sentinelle)
                node = node.gauche;
            return node;
        }


        /**
         * Renvoie le successeur de ce noeud
         *
         * @return le noeud contenant la clé qui suit la clé de ce noeud dans l'ordre
         *     des clés, null si c'es le noeud contenant la plus grande clé
         */

        Noeud suivant() {
            Noeud r;
            Noeud node = this;
            if (node.droit != sentinelle) {
                return (node.droit).minimum();
            } else {
                r= node.pere;
                while (r!= sentinelle && node == r.droit) {
                    node = r;
                    r= r.pere;
                }
                return r;
            }
        }

    }

    // Consructeurs

    /**
     * Créer un arbre vide. Les éléments sont ordonnés selon l'ordre naturel
     */

    public ARB_RN() {
        taille = 0;
        sentinelle = new Noeud();
        racine = sentinelle;
        cmp = new Comparator<E>() {
            public int compare(E e, E e1) {
                return ((Comparable<E>) e).compareTo(e1);
            }
        };
    }

    /**
     * Crée un arbre vide. Les éléments sont comparés selon l'ordre imposé par le comparateur
     *
     * @param comparateur le comparateur utilisé pour définir l'ordre des éléments
     */
    public ARB_RN(Comparator<? super E> cmp) {
        taille = 0;
        sentinelle = new Noeud();
        racine = sentinelle;
        this.cmp = cmp;
    }

    /**
     * Constructeur par recopie. Crée un arbre qui contient les mÃªmes éléments
     * que c. L'ordre des éléments est l'ordre naturel.
     *
     * @param c la collection Ã  copier
     */

    public ARB_RN(Collection<? extends E> co) {
        this();
        addAll(co);
    }

    /*
    cette methode pour faire la rotation des noeuds sur la
    direction droite vers gauche
     */
    /**
     * @param n le noeud a partir du quel on fait la rotation
     */
    private void rotationGauche(Noeud node) {
        Noeud r = node.droit;
        node.droit = r.gauche;
        if (!r.gauche.isSentinelle())
            r.gauche.pere = node;
        r.pere = node.pere;
        if (node.pere == sentinelle)
            racine = r;
        else if (node.pere.gauche == node)
            node.pere.gauche = r; 
        else
            node.pere.droit = r;
        r.gauche = node;
        node.pere = r;
    }

    /*
    cette methode pour faire la rotation des noeuds sur la
    direction gauche vers droite
     */

    /**(idem que rotation gauche)
     *
     * @param n le noeud apartir du quel on fait la rotation
     */

    private void rotationDroite(Noeud node) {
        Noeud r = node.gauche;
        node.gauche = r.droit;
        if (!r.droit.isSentinelle())
            r.droit.pere = node;
        r.pere = node.pere;
        if (node.pere == sentinelle)
            racine = r;
        else if (r.pere.droit == node)
            node.pere.droit = r;
        else
            node.pere.gauche = r;
        r.droit = node;
        node.pere = r;
    }

    /*
    Fait la réorganisation insertion après chaque appel de la
      méthode insertion et qui  suivre des cas précisée :
     */

    /**
     * reorganisation de l'arbre, en remontant vers la racine
     * elle utilise les méthodes rotation droit et rotation gauche
     * @param n apartir du quel on débute la reorganisation
     */
    private void reorganize_insert(Noeud node) {
        while (node != racine && node.pere.couleur == Color.RED) {
            if (node.pere == node.pere.pere.gauche) {
                Noeud r= node.pere.pere.droit;
                if (r.couleur == Color.RED) {
                    node.pere.couleur = Color.BLACK;
                    r.couleur = Color.BLACK;
                    node.pere.pere.couleur = Color.RED;
                    node = node.pere.pere;
                } else {
                    if (node == node.pere.droit) {
                        node = node.pere;
                        rotationGauche(node);
                    }
                    node.pere.couleur = Color.BLACK;
                    node.pere.pere.couleur = Color.RED;
                    rotationDroite(node.pere.pere);
                }
            } else {
                Noeud r = node.pere.pere.gauche;
                if (r.couleur == Color.RED) {
                    node.pere.couleur = Color.BLACK;
                    r.couleur = Color.BLACK;
                    node.pere.pere.couleur = Color.RED;
                    node = node.pere.pere;
                } else {
                    if (node== node.pere.gauche) {
                        node = node.pere;
                        rotationDroite(node);
                    }
                    node.pere.couleur = Color.BLACK;
                    node.pere.pere.couleur = Color.RED;
                    rotationGauche(node.pere.pere);
                }
            }
        }
        racine.couleur = Color.BLACK; // pour que la racine reste toujours noir
    }

    /**
     * inserer un element(noeud) dans l'arbre
     *
     * @param nouveau
     */

    public void inserer(Noeud no) {
        Noeud r = sentinelle;
        Noeud node = racine;
        while (node!= sentinelle) {
            r= node; 
            if (cmp.compare(no.cle, node.cle) < 0) {
                node = node.gauche;
            } else
                node = node.droit;
        }
        no.pere = r;
        if (r == sentinelle) {
            racine = no;
        } else {
            if (cmp.compare(no.cle, r.cle) < 0) {
                r.gauche = no;
            } else
                r.droit = no;
        }
        no.gauche = no.droit = sentinelle;

    }
    /**
     * add pour addAll
     *
     * @param
     */

    public boolean add(E cle) {
        Noeud no = new Noeud(cle);
        inserer(no);
        reorganize_insert(no);
        return true;
    }

    // @Override
    public Iterator<E> iterator() {
        return new ABRIterator();
    }
    /**
     * @return la taille
     */
    // @Override
    public int size() {
        return taille;
    }


    /**
     * Recherche une Noeud.
     * @param  z Le Noeud a chercher
     * @return le noeud qui contient la clé ou null si la clé n'est pas trouvée.
     */

    private Noeud rechercher(Noeud no) {
        Noeud node= racine;
        while (node != sentinelle && cmp.compare(node.cle, no.cle) != 0) {
            if (cmp.compare(no.cle, node.cle) < 0) {
                node = node.gauche;
            } else
               node = node.droit;
        }
        return node;
    }
    /**
     * @param  o L'object a chercher
     * @return un boulean, vrai si l'arbre contient un noeud dans l'objet(clé) est o
     */

    public boolean contains(Object ob) {
        Noeud n= new Noeud((E) ob);
        if (rechercher(n) != sentinelle) {
            return true;
        }
        return false;
    }

    /**
     * Supprime le noeud n.
     *
     * @param n le noeud a supprimer
     * @return le noeud contenant la clé qui suit celle de z dans l'ordre des
     *         clés. Cette valeur de retour peut Ãªtre utile dans
     *         {@link Iterator#remove()} si le noeud z est une feuille, alors on
     *         l'élimine; si le noeud n possede un seul fils, on élimine z et
     *         on « remonte » ce fils. si le noeud z possede deux fils, on
     *         cherche le prédécesseur t de n. Celui-ci n'a pas de fils droit.
     *         On remplace le contenu de n par le contenu de t, et on élimine t.
     */

    private Noeud supprimer(Noeud n) {
        Noeud r, node;
        if (n.gauche == sentinelle || n.droit == sentinelle) {
            r = n;
        } else {
            r = n.suivant();
        }

        if (r.gauche != sentinelle) {
            node = r.gauche;
        } else
           node = r.droit;
        node.pere = r.pere;
        if (r.pere == sentinelle)
            racine = node;
        else {
            if (r == r.pere.gauche)
                r.pere.gauche = node;
            else
                r.pere.droit = node;
        }
        if (cmp.compare(r.cle, n.cle) != 0)
            n.cle = r.cle;
        recycler(r);
        if ( n.couleur == Color.BLACK)
            reorganize_delete ( node );
        return node;
    }

    /** (idem)
     * reorganisation de l'arbre, en remontant vers la racine
     * elle utilise les méthodes rotation droit et rotation gauche
     * @param n apartir du quel on débute la reorganisation
     */

    private void reorganize_delete (Noeud node) {
        while (node != racine && node.couleur == Color.BLACK) {
            if (node == node.pere.gauche) {
                Noeud r = node.pere.droit;
                if (r.couleur == Color.RED) {
                    r.couleur = Color.BLACK;
                    node.pere.couleur = Color.RED;
                    rotationGauche(node.pere);
                   r = node.pere.droit;
                }
                if (r.gauche.couleur == Color.BLACK && r.droit.couleur == Color.BLACK) {
                    r.couleur = Color.RED;
                    node = node.pere;
                } else {
                    if (r.droit.couleur == Color.BLACK) {
                        r.gauche.couleur = Color.BLACK;
                        r.couleur = Color.RED;
                        rotationDroite(r);
                        r = node.pere.droit;
                    }
                    r.couleur = node.pere.couleur;
                    node.pere.couleur = Color.BLACK;
                    r.droit.couleur = Color.BLACK;
                    rotationGauche(node.pere);
                    break;
                }
            } else {
                Noeud r = node.pere.gauche;
                if (r.couleur == Color.RED) {
                    r.couleur = Color.BLACK;
                    node.pere.couleur = Color.RED;
                    rotationDroite(node.pere);
                    r= node.pere.gauche;
                }
                if (r.droit.couleur == Color.BLACK && r.gauche.couleur == Color.BLACK) {
                  
                    r.couleur = Color.RED;
                    node = node.pere;
                } else {
                    if (r.gauche.couleur == Color.BLACK) {
                        r.droit.couleur = Color.BLACK;
                        r.couleur = Color.RED;
                        rotationGauche(r);
                       r = node.pere.gauche;
                    }
                   r.couleur = node.pere.couleur;
                    node.pere.couleur = Color.BLACK;
                    r.gauche.couleur = Color.BLACK;
                    rotationDroite(node.pere);
                    break;
                }
            }
        }
        node.couleur = Color.BLACK;  // our rester la racine toujour noir
    }

    /**
     * @param o l'objet a suprimé
     * @return vrai, s'il l'objet a été supprimé
     * elle fait appel a la methode rechercher si l'objet existe dans notre arbre
     * si oui elle fait appel a supprimer
     */

    public boolean remove(Object ob) {
        Noeud k = new Noeud((E) ob);
        Noeud node = rechercher(k);

        if (node == sentinelle)
            return false;
        else if (supprimer(node) != sentinelle);
        return true;
    }

    /**
     *
     * @param x le noeud a mettre a null
     * cette methode est utlisée par la méthode supprimer pour supprimer un noeud
     */

    public void recycler(Noeud node) {
        node.pere = sentinelle;
        node.gauche = sentinelle;
        node.droit = sentinelle;
    }


    /**
     * Les itérateurs doivent parcourir les éléments dans l'ordre ! Ceci peut se
     * faire facilement en utilisant {@link Noeud#minimum()} et
     * {@link Noeud#suivant()}
     */

    private class ABRIterator implements Iterator<E> {
        private Noeud pos;                  // Noeud pour la position
        private Noeud prece;                // Noeud pour le précédant

        ABRIterator() {
            pos = racine.minimum();
            prece = sentinelle;
        }

        public boolean hasNext() {
            return (pos != sentinelle);
        }

        public E next() {
            if (pos == sentinelle)
                throw new NoSuchElementException(" plus d'élements ");
            prece = pos;
            pos = pos.suivant();
            return prece.cle;
        }

        public void remove() {
            if (prece == sentinelle)
                throw new IllegalStateException(" next() n'a pas été appelée ");

            pos = supprimer(prece);
            prece = sentinelle;

        }
    }

    /* Pour un meilleur affichage
     * @see java.util.AbstractCollection#toString()
     */

    // @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        toString(racine, buf, "", maxStrLen(racine));
        return buf.toString();
    }

    private void toString(Noeud x, StringBuffer buf, String path, int len) {
        if (x == sentinelle) {
            return;
        }
        toString(x.droit, buf, path + "D", len);
        for (int i = 0; i < path.length(); i++) {
            for (int j = 0; j < len + 6; j++)
                buf.append(' ');
            char c = ' ';
            if (i == path.length() - 1)
                c = '+';
            else if (path.charAt(i) != path.charAt(i + 1))
                c = '|';
            buf.append(c);
        }
        if (x.couleur == Color.BLACK)
            buf.append("-- " + x.cle.toString() + "NOIR");
        else
            buf.append("-- " + x.cle.toString() + "RED");
        if (x.gauche != sentinelle || x.droit != sentinelle) {
            buf.append(" --");
            for (int j = x.cle.toString().length(); j < len; j++)
                buf.append('-');
            buf.append('|');
        }
        buf.append("\n");
        toString(x.gauche, buf, path + "G", len);
    }

    private int maxStrLen(Noeud x) {
        return x == sentinelle ? 0
                : Math.max(x.cle.toString().length(), Math.max(maxStrLen(x.gauche), maxStrLen(x.droit)));
    }

    
	//Le Main 
    public static void main(String[] args) {
    	System.out.println("\n------------------------> DEBUT DU PROGRAMME <------------------------");
        ARB_RN<Integer> arb = new ARB_RN<Integer>();

        // On va ajouter un seul Noeud
        arb.add(20);

        // afficher l'arbre
        System.out.println(arb.toString());
        System.out.println("-----------------------");

        // On supprime le noeud qu'on a ajoute
        System.out.println("on supprime le noeud que on vient ajoute ");
        arb.remove(20);
        System.out.println(arb.toString());
        System.out.println("-----------------------");

        // On ajouter ces ensembles des valeurs part 1

        int[] val = { 20, 12, 45, 50,32,55 };
        for (int i = 0; i < val.length; i++) {
            arb.add(val[i]);
        }
        System.out.println("on a ajoute un tableau des valeurs ");
        System.out.println(arb.toString());
        System.out.println("-----------------------");

        // on supprimer la tete
        System.out.println("on supprime la tete ");
        arb.remove(20);
        System.out.println(arb.toString());
        System.out.println("-----------------------");


        // on ajouter une ensemble des valeur ordonnée croissant part 2
        int[] val1 = { 66 , 80 , 88,89,90,99 };
        for (int i = 0; i < val.length; i++) {
            arb.add(val1[i]);
        }
        System.out.println("on a ajoute un tableau d'ensemble des valeurs ordonnée croissant ");
        System.out.println(arb.toString());
        System.out.println("-----------------------");
	//System.out.println("Taille de l'arbre :");
	//System.out.println(arb.size());

        // on supprimer plus qu'une valeur

        System.out.println("on supprimer des valeur  ");
        arb.remove(88);
        arb.remove(32);
        System.out.println(arb.toString());
        System.out.println("-----------------------");

        // on utilisier l'eterateur pour supprimer

        System.out.println("on utilise l'iterateur pour supprimer   ");
		Iterator<Integer> it = arb.iterator();
		for (int i = 0; i < 1; i++)
			it.next();
        		it.remove();
        System.out.println(arb.toString());
        System.out.println("-----------------------");
	/*System.out.println("Taille de l'arbre :");
	System.out.println(arb.size());*/
	
	
	
	

	System.out.println("\n------------------------> FIN DU PROGRAMME <------------------------");
	
	}
    }
